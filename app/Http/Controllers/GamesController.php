<?php

namespace App\Http\Controllers;

use App\Traits\FormatViewData;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class GamesController extends Controller
{
    use FormatViewData;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $game = Cache::remember('game', 86400, function () use ($slug) {

            return Http::withHeaders(config('services.igdb'))
                ->withBody(
                    "
                        fields name, cover.url, first_release_date, platforms.abbreviation, rating,
                        slug, involved_companies.company.name, genres.name, aggregated_rating, summary, websites.*, videos.*, 
                        screenshots.*, similar_games, similar_games.cover.url, similar_games.name, similar_games.rating,
                        similar_games.platforms.abbreviation, similar_games.slug;
                        where slug=\"{$slug}\";
                    ",
                    'text/plain'
                )
                ->post('https://api.igdb.com/v4/games')
                ->json();
        });

        // Check if array is empty - game not found
        abort_if(!$game, 404);

        return view('show', [
            'game' => $this->formatGame($game[0]),
        ]);
    }
}
