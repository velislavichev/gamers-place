<?php

namespace App\Http\Livewire;

use App\Traits\FormatViewData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Livewire\Component;
class ComingSoon extends Component
{
    use FormatViewData;

    /**
     * 
     * @var array
     */
    public $comingSoon = [];

    /**
     * Load comming soon games
     *
     * @return void
     */
    public function loadComingSoon()
    {
        $current = Carbon::now()->timestamp;

        $comingSoonUnformatted = Cache::remember('coming-soon', 86400, function () use ($current) {

            return Http::withHeaders(config('services.igdb'))
                ->withBody(
                    "
                        fields name, cover.url, first_release_date, platforms.abbreviation, rating, rating_count, summary, slug;
                        where platforms = (48,49,130,6)
                        & (first_release_date >= {$current}
                        & rating > 5);
                        where rating != null;
                        sort first_release_date asc;
                        limit 4;
                    ",
                    'text/plain'
                )->post('https://api.igdb.com/v4/games')
                ->json();
        });

        $this->comingSoon = $this->formatComingSoon($comingSoonUnformatted);
    }

    /**
     * 
     * @return view
     */
    public function render()
    {
        return view('livewire.coming-soon');
    }
}
