<?php

namespace App\Http\Livewire;

use App\Traits\FormatViewData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class MostAnticipated extends Component
{
    use FormatViewData;

    /**
     * 
     * @var array
     */
    public $mostAnticipated = [];

    /**
     * Load most anticipated games
     *
     * @return void
     */
    public function loadMostAnticipated()
    {
        $current = Carbon::now()->timestamp;
        $afterFourMonths = Carbon::now()->addMonths(4)->timestamp;

        $mostAnticipatedUnformatted = Cache::remember('most-anticipated', 86400, function () use ($current, $afterFourMonths) {

            return Http::withHeaders(config('services.igdb'))
                ->withBody(
                    "
                        fields name, cover.url, first_release_date, platforms.abbreviation, rating, rating_count, summary, slug;
                        where platforms = (48,49,130,6)
                        & (first_release_date >= {$current}
                        & first_release_date < {$afterFourMonths});
                        sort rating desc;
                        where rating != null;
                        limit 4;
                    ",
                    'text/plain'
                )->post('https://api.igdb.com/v4/games')
                ->json();
        });

        $this->mostAnticipated = $this->formatMostAnticipated($mostAnticipatedUnformatted);
    }

    /**
     * 
     * @return view
     */
    public function render()
    {
        return view('livewire.most-anticipated');
    }
}
