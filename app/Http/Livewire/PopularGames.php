<?php

namespace App\Http\Livewire;

use App\Traits\FormatViewData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class PopularGames extends Component
{
    use FormatViewData;

    /**
     * 
     * @var array
     */
    public $popularGames = [];

    /**
     * Load popular games
     *
     * @return void
     */
    public function loadPopularGames()
    {
        $before = Carbon::now()->subMonths(2)->timestamp;
        $after = Carbon::now()->addMonths(2)->timestamp;

        $popularGamesUnformatted = Cache::remember('popular-games', 86400, function () use ($before, $after) {
            return Http::withHeaders(config('services.igdb'))
                ->withBody(
                    "
                        fields name, cover.url, first_release_date, platforms.abbreviation, rating, slug;
                        where platforms = (48,49,130,6)
                        & (first_release_date >= {$before}
                        & first_release_date < {$after});
                        sort rating desc;
                        where rating != null;
                        limit 12;
                    ",
                    'text/plain'
                )
                ->post('https://api.igdb.com/v4/games')
                ->json();
        });

        $this->popularGames = $this->formatPopularGames($popularGamesUnformatted);

        collect($this->popularGames)->filter(function ($game) {
            return $game['rating'];
        })->each(function ($game) {
            $this->emit(
                'gameWithRatingAdded',
                [
                    'slug' => $game['slug'],
                    'rating' => $game['rating'] / 100,
                ]
            );
        });
    }

    /**
     * 
     * @return view
     */
    public function render()
    {
        return view('livewire.popular-games');
    }
}
