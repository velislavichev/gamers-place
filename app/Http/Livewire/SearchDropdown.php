<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class SearchDropdown extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $searchResults = [];

    /**
     * Return search dropdown data
     *
     * @return view
     */
    public function render()
    {
        $this->searchResults = Http::withHeaders(config('services.igdb'))
            ->withBody(
                "
                    search \"{$this->search}\";
                    fields name, slug, cover.url;
                    limit 8;
                ",
                'text/plain'
            )
            ->post('https://api.igdb.com/v4/games')
            ->json();

        return view('livewire.search-dropdown');
    }
}
