<?php

namespace App\Http\Livewire;

use App\Traits\FormatViewData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class RecentlyReviewed extends Component
{
    use FormatViewData;

    public $recentlyReviewed = [];

    /**
     * Load recently reviewed games
     *
     * @return void
     */
    public function loadRecentlyReviewed()
    {
        $before = Carbon::now()->subMonths(2)->timestamp;
        $current = Carbon::now()->timestamp;

        $recentlyReviewedUnformatted  = Cache::remember('recently-reviewed', 86400, function () use ($before, $current) {
            return Http::withHeaders(config('services.igdb'))
                ->withBody(
                    "
                        fields name, cover.url, first_release_date, platforms.abbreviation, rating, rating_count, summary, slug;
                        where platforms = (48,49,130,6)
                        & (first_release_date >= {$before}
                        & first_release_date < {$current}
                        & rating_count > 5);
                        sort rating desc;
                        where rating != null;
                        limit 3;
                    ",
                    'text/plain'
                )->post('https://api.igdb.com/v4/games')
                ->json();
        });

        $this->recentlyReviewed = $this->formatRecentlyViewed($recentlyReviewedUnformatted);

        collect($this->recentlyReviewed)->filter(function ($game) {
            return $game['rating'];
        })->each(function ($game) {
            $this->emit('reviewGameWithRatingAdded', [
                'slug' => 'review_' . $game['slug'],
                'rating' => $game['rating'] / 100
            ]);
        });
    }

    /**
     * 
     * @return view
     */
    public function render()
    {
        return view('livewire.recently-reviewed');
    }
}
