@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 text-center sm:text-justify ">
        <h2 class="text-blue-500 uppercase tracking-wide font-semibold">
            Popular Games
        </h2>

        <!-- Most popular section -->
        @livewire('popular-games')

        <div class="flex flex-col lg:flex-row my-10">
            <div class="recently-reviewed w-full lg:w-3/4 mr-0 lg:mr-32">
                <h2 class="text-blue-500 uppercase tracking-wide font-semibold">Recently Reviewed</h2>

                <!-- Recently reviewd section -->
                @livewire('recently-reviewed')
            </div>

            <div class="most-anticipated w-full lg:w-1/4">

                <h2 class="text-blue-500 uppercase tracking-wide font-semibold mt-24 lg:mt-0">Most Anticipated</h2>

                <!-- Most anticipated section -->
                @livewire('most-anticipated')

                <h2 class="text-blue-500 uppercase tracking-wide font-semibold mt-8">Coming Soon</h2>

                <!-- Coming soon section -->
                @livewire('coming-soon')

            </div>
        </div>
    </div>
@endsection
