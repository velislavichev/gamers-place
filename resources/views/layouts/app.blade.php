<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">



    <!-- Livewire CSS -->
    @livewireStyles

    <!-- Alpine.js -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    <title>Video games project</title>
</head>

<body class="bg-gray-900 text-white">
    <header class="border-b border-grey-800">
        <nav class="container mx-auto flex flex-col lg:flex-row items-center justify-between px-4 py-6">

            <a href="/" class="flex flex-col lg:flex-row items-center">
                <img src="/igdb-logo.png" alt="video games" class="w-16 flex-none">

                <p class="lg:ml-4 mt-2 ">
                    IGDB Games
                </p>
            </a>

            <div class="flex items-center mt-6 lg:mt-0">
                @livewire('search-dropdown')
            </div>

        </nav>
    </header>

    <main class="py-8">
        @yield('content')
    </main>

    <footer class="border-t border-gray-800">
        <div class="container mx-auto px-4 py-6 text-center">
            Powered By <a href="https://www.igdb.com/api" target="_blank" class="underline hover:text-gray-400">IGDB API</a>
        </div>
    </footer>

    <!-- Livewire JS -->
    @livewireScripts
    <script src="/js/app.js"></script>

    @stack('scripts')
</body>

</html>
