<div wire:init="loadMostAnticipated">

    @forelse ($mostAnticipated as $game)

        <div class="most-anticipated-container space-y-10 mt-8">
            <div class="game flex">
                <a href="{{ route('games.show', $game['slug']) }}">
                    <img src="{{ $game['coverImageUrl'] }}" alt="game cover"
                        class="w-20 hover:opacity-75 transition ease-in-out duration-150">
                </a>
                <div class="ml-4">
                    <a href="{{ route('games.show', $game['slug']) }}" class="hover:text-gray-300"> {{ $game['name'] }}</a>
                    <div class="text-gray-400 text-small mt-1">
                        {{ $game['releaseDate'] }}
                    </div>
                </div>
            </div>

        </div>
    @empty

        @foreach (range(1, 4) as $game)

            <div class="most-anticipated-container animate-pulse space-y-10 mt-8">

                <div class="game flex">
                    <div class="bg-gray-800 w-20 h-20 flex-none"></div>

                    <div class="ml-4">
                        <div class="text-transparent bg-gray-700 rounded leading-tight"> Title video game</div>

                        <div class="text-transparent bg-gray-700 rounded inline-block text-small mt-2">
                            Sept 10, 2020
                        </div>
                    </div>
                </div>
            </div>

        @endforeach

    @endforelse

</div>
