<div wire:init="loadPopularGames"
    class="popular-games text-sm grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6 gap-12 pb-16 border-b border-gray-800">

    @forelse ($popularGames as $game)

        <div class="game mt-8">
            <div class="relative inline-block">
                <a href="{{ route('games.show', $game['slug']) }}">
                    <img src="{{ $game['coverImageUrl'] }}" alt="game cover"
                        class="hover:opacity-75 transition ease-in-out duration-150">
                </a>

                @if ($game['rating'])

                    <div id="{{ $game['slug'] }}" class="absolute bottom-0 right-0 w-16 h-16 bg-gray-800 rounded-full"
                        style="bottom: -20px; right:-20px">
                    </div>

                @endif

            </div>

            <a href="{{ route('games.show', $game['slug']) }}" class="block text-base font-semibold leading-tight hover:text-gray-400 mt-8">
                {{ $game['name'] }}
            </a>
            <div class="text-gray-400 mt-1">
                {{ $game['platforms'] }}
            </div>
        </div>

    @empty

        @foreach (range(1, 12) as $game)

            <div class="animate-pulse game mt-8">
                <div class="relative inline-block">
                    <div class="bg-gray-800 w-44 h-56"></div>
                </div>

                <a href="#" class="block text-transparent text-lg bg-gray-700 leading-tight mt-8">
                    Title video game
                </a>

                <div class="text-transparent bg-gray-700 rounded inline-block mt-3">
                    ps 4, ps, switch
                </div>
            </div>

        @endforeach

    @endforelse

</div>

@push('scripts')
    @include('_rating', [
        'event' => 'gameWithRatingAdded'
    ])
@endpush
